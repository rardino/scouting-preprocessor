# Template for file containing paths and variables for Vivado and Modelsim.
# Replace and uncomment the environment variables below, then remove '_template' from the file name.
## Setup Vivado
# export XILINXD_LICENSE_FILE=''
# source [path to Vivado installation]/Vivado[version]/Vivado/[version]/settings64.sh
## Setup Modelsim
# export MODELSIM_ROOT='[path to Modelsim installation]/modelsim-106a/modeltech/'
## License servers
# export MGLS_LICENSE_FILE=''
export PATH=$PATH:${MODELSIM_ROOT}/linux_x86_64
export BUILD_DIR="build/"
