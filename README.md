# 40 MHz scouting preprocessor

This board receives trigger data via 4-8 optical links @ 10 Gb/s and provides their data to an attached CPU-based compute system.

## Requirements

The `ipbb` build environment requires Python 2.7, pip, and virtualenv to run. For the firmware build itself Xilinx Vivado (at least 2018.3) and for the simulation Mentor Modelsim 10.6a are used.

## Build instructions

First remember to set your BUILD_DIR of choice.

From the scouting-preprocessor directory level the project can be set up with:

```bash
scripts/generate_addr_table.py scouting-board/firmware/addrtab/address_table.json scouting-board/firmware/hdl/common/decoder_constants_kcu1500.vhd [Demux|uGMT]
scripts/makeScout[Demux|Ugmt]Project.sh
```

for the UGMT and Calo demux versions of the code.

A bitfile can be generated in the following way:

```bash
scripts/buildFirmware.sh
```

It can then be found in the folder `build/scouting/proj/scouting_build/package`.

*Note:* If the build scripts find the file `buildToolSetup.sh` in the base directory they will source it. It can be used to set the environment variables required for Vivado and Modelsim. A template (`buildToolSetup_template.sh`) is provided.

## Versioning

The scouting boards follow semantic versioning for which the 24 LSBs of the algorithm rev field are used. These are segmented into three eight bit fields, indicating major, minor, patch level.

The eight MSBs indicate the scouting board type where the following codes are currently in use:
* `0x0` ... Prototype board until late 2021
* `0x1` ... uGMT inputs
* `0x2` ... Layer-2 Demux inputs
* `0x5` ... uGMT inputs, bril histogramming enabled
