#!/usr/bin/env bash

set -e # Exit on error

echo "Now loading bitfile, this could take a moment.."
vivado_lab -mode batch -source /home/scouter/bitfiles/currently_used/vivado_load_bitfile.tcl
# Rescan PCIe bus
echo "Rescanning the PCIe bus"
echo "### ATTENTION: You may be asked for the password of user *scouter* now!"
sudo /usr/sbin/pcie_reconnect_xilinx.sh
