#!/bin/bash
set -e # Exit on error.
if [ -f buildToolSetup.sh ]; then
    source buildToolSetup.sh
fi
if [ -z ${XILINX_VIVADO:+x} ]; then
    echo "Xilinx Vivado environment has not been sourced. Exiting."
    exit 1
else
    echo "Found Xilinx Vivado at" ${XILINX_VIVADO}
fi
BASE_DIR=$(pwd)

# Generate decoder
cp scouting-board/firmware/addrtab/address_table_demux.json scouting-board/firmware/addrtab/address_table.json
scripts/generate_decoder.py scouting-board/firmware/addrtab/address_table.json scouting-board/firmware/hdl/common/decoder_constants_kcu1500.vhd Demux

mkdir $BUILD_DIR
cd $BUILD_DIR
ipbb init scouting
mkdir scouting/src/scouting-preprocessor
pushd scouting/src/scouting-preprocessor
ln -sf ../../../../scouting-board
popd
pushd scouting
ipbb proj create vivado scouting_build scouting-preprocessor:scouting-board top_scouting_demux.dep
ipbb toolbox check-dep vivado scouting-preprocessor:scouting-board top_scouting_demux.dep
popd
