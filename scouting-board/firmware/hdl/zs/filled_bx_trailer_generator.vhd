library IEEE;
use IEEE.STD_LOGIC_1164.all;
use work.datatypes.all;

use IEEE.NUMERIC_STD.all;

entity filled_bx_trailer_generator is
    generic (
        NSTREAMS : integer
    );
    port (
        clk    : in std_logic;
        rst    : in std_logic;
        d      : in adata(NSTREAMS - 1 downto 0);
        d_ctrl : in acontrol;
        q      : out adata(NSTREAMS - 1 downto 0);
        q_ctrl : out acontrol
    );
end filled_bx_trailer_generator;

architecture Behavioral of filled_bx_trailer_generator is

    signal filled_bxs       : std_logic_vector(14 * NSTREAMS * 32 downto 1); -- 3564 bits fit in 14 frames on 8 links.
    signal filled_bxs_store : std_logic_vector(14 * NSTREAMS * 32 downto 1); -- 3564 bits fit in 14 frames on 8 links.
    signal start_trailer    : std_logic;                                     -- Start signal for the trailer

begin

    filled_bxs(filled_bxs'high downto 3565) <= (others => '0'); -- Fill remaining space in last frame with zeros.

    gen_trailer : process (clk, rst) is
        variable bx_counter : integer range 1 to 3564; -- 3564 BXs per orbit 
    begin
        if rst = '1' then
            filled_bxs(3564 downto 1) <= (others => '0'); -- Zero filled_bx field at reset.
        elsif rising_edge(clk) then
            if d_ctrl.valid = '0' then
                filled_bxs(3564 downto 1) <= (others => '0'); -- Zero filled_bx field in valid gap.
            elsif d_ctrl.bx_start = '1' then
                -- If we didn't suppress the BX we mark that in the filled_bxs vector.
                if d_ctrl.strobe = '1' then
                    filled_bxs(d_ctrl.bx_counter) <= '1';
                else
                    filled_bxs(d_ctrl.bx_counter) <= '0';
                end if;
            end if;
        end if;
    end process;

    gen_output : process (clk, rst) is
        variable trailer_frame_counter : integer range 0 to 17; -- We use three BXs for the trailer. The very last frame is added by the FIFO filler downstream.
    begin
        if rst = '1' then
            trailer_frame_counter := 17; -- Begin in disabled state.
            start_trailer <= '0';
        elsif rising_edge(clk) then

            if d_ctrl.last = '1' then
                start_trailer    <= '1'; -- If we saw the last frame we should make the trailer in the next tick.
                filled_bxs_store <= filled_bxs;
            end if;

            if start_trailer = '1' then
                start_trailer <= '0';
                trailer_frame_counter := 0;
            elsif trailer_frame_counter < 17 then
                trailer_frame_counter := trailer_frame_counter + 1;
            end if;

            for chan in q'range loop
                if trailer_frame_counter = 0 then -- Mark beginning of trailer.
                    q(chan) <= x"beefdead";
                elsif trailer_frame_counter < 15 then -- Generate filled BX map.
                    q(chan) <= filled_bxs_store((32 * NSTREAMS * (trailer_frame_counter - 1)) + (32 * chan) + 32 downto (32 * NSTREAMS * (trailer_frame_counter - 1)) + (32 * chan) + 1);
                elsif trailer_frame_counter = 15 then -- Label channels.
                    q(chan) <= std_logic_vector(to_unsigned(chan, 32));
                elsif trailer_frame_counter < 17 then -- For future use. Fill up with zeros.
                    q(chan) <= (others => '0');
                else -- Not in the header, forward what we got in.
                    q(chan) <= d(chan);
                end if;
            end loop;

            if d_ctrl.last = '1' then -- Intercept previously generated last bit.
                q_ctrl.last       <= '0';
                q_ctrl.valid      <= d_ctrl.valid;
                q_ctrl.strobe     <= d_ctrl.strobe;
                q_ctrl.bx_start   <= d_ctrl.bx_start;
                q_ctrl.bx_counter <= d_ctrl.bx_counter;
            elsif trailer_frame_counter < 17 then -- Sending trailer. Override valid, strobe and last bit.
                q_ctrl.valid  <= '1';
                q_ctrl.strobe <= '1';
                if trailer_frame_counter = 16 then -- Last word in generated trailer.
                    q_ctrl.last <= '1';
                else
                    q_ctrl.last <= '0';
                end if;
                q_ctrl.bx_start   <= d_ctrl.bx_start;
                q_ctrl.bx_counter <= d_ctrl.bx_counter;
            else
                q_ctrl <= d_ctrl;
            end if;

        end if;
    end process;

end Behavioral;
