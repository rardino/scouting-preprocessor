----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Hannes Sakulin
-- 
-- Create Date: 09/19/2018 10:15:25 AM
-- Design Name: 
-- Module Name: tb_aligner - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

use work.top_decl.all;
use work.datatypes.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_aligner is
--  Port ( );
end tb_aligner;

architecture Behavioral of tb_aligner is
  constant t_period : time := 25 ns;
  constant t_halfperiod : time := t_period / 2;
  constant t_hold : time := t_period / 5;

  
  constant BUFFER_SIZE : integer := 64;
  constant NSTREAMS    : integer := 4;
  constant ORBIT       : integer := 450;
  
  signal I2C_MAIN_RESET_B_LS : STD_LOGIC;
  
  signal clk           : STD_LOGIC := '1';
  signal rst_global    : std_logic;
  signal rst_algo, rst_pll, rst_tx, rst_rx : std_logic;
  signal rst_i2c, str_wr : std_logic;
  signal rst_packager, rstn_fifo : std_logic;
  signal start_align   : STD_LOGIC;
  signal q  : ldata(NSTREAMS-1 downto 0);
  signal outputstreams : ldata(NSTREAMS-1 downto 0);
  
  signal measured_delays, measured_delays_pre : delay_vector(NSTREAMS-1 downto 0);
  signal commas_began, commas_began_pre       : std_logic_vector(NSTREAMS-1 downto 0);

  signal bc0, aligned, aligned_pre : std_logic;

    signal ocounter : natural := 0;
    signal bcounter : natural := 0;
    signal evt_word : natural range 0 to 5 := 0;
    
    signal test_tdata    : std_logic_vector(255 downto 0);
    signal test_tlast    : std_logic;
    signal test_tvalid   : std_logic;
    signal test_tready   : std_logic;

    signal d_delayed : ldata(NSTREAMS-1 downto 0);
    signal finding_delays_o : std_logic_vector(NSTREAMS-1 downto 0);
    
    signal state    : state_vec(NSTREAMS-1 downto 0);
    signal word_cnt : TWordCnt(NSTREAMS-1 downto 0);
    signal orbit_start : std_logic_vector(NSTREAMS-1 downto 0);
    signal rd_point : TReadPointer(NSTREAMS-1 downto 0);
    signal d_o      : ldata(NSTREAMS-1 downto 0);
    signal q_int    : ldata(NSTREAMS-1 downto 0);
    signal d_buf    : TLinkBuffer(NSTREAMS-1 downto 0);
    signal q_buffer : TLinkBuffer(5 downto 0);    
begin
    -- Clock
    clk  <= not clk  after t_halfperiod;

  global_reset : entity work.reset
  port map (
      clk_free     => clk,
      clk_i2c      => clk,
      clk_algo     => clk,
      rst_global   => rst_global,
      enable_i2c   => I2C_MAIN_RESET_B_LS,
      rst_i2c      => rst_i2c,
      write_i2c    => str_wr,
      rst_pll      => rst_pll,
      rst_tx       => rst_tx,
      rst_rx       => rst_rx,
      rst_algo     => rst_algo,
      rst_packager => rst_packager,
      rstn_fifo    => rstn_fifo
      );

    align_mon_pre : entity work.align_monitor
        generic map (
            QUADS => NSTREAMS/4
        )
        port map (
            clk             => clk,
            rst             => rst_algo,
            d               => q,
            bc0             => bc0,
            measured_delays => measured_delays_pre,
            aligned         => aligned_pre,
            commas_began_out => commas_began_pre
        );

  aligner_1: entity work.aligner
    generic map (
      BUFFER_SIZE => BUFFER_SIZE,
      NSTREAMS    => NSTREAMS)
    port map (
      clk           => clk,
      rst         => rst_algo,
      d => q,
      q => outputstreams);

    align_mon : entity work.align_monitor
    generic map (
        QUADS => NSTREAMS/4
    )
    port map (
        clk             => clk,
        rst             => rst_algo,
        d               => outputstreams,
        bc0             => bc0,
        measured_delays => measured_delays,
        aligned         => aligned,
        commas_began_out => commas_began,
        finding_delays_o => finding_delays_o
    );

    axi_packer : entity work.axi_packager
        generic map ( 
                ORBIT => ORBIT,
                QUADS => NSTREAMS/4
                      )
        port map ( 
                clk     => clk,
                rst     => rst_packager,
                d       => outputstreams,
                tready  => test_tready,
                rx_data => test_tdata,
                tvalid  => test_tvalid,
                tlast   => test_tlast,
                state_out => state,
                word_cnt => word_cnt,
                orbit_st => orbit_start,
                rd_point => rd_point,
                d_o => d_o,
                d_buf => d_buf
               );


--    assign_data : process(clk)
--        begin
--            if clk'event and clk = '1' and test_tready = '1' then -- Make sure we react properly to ready signal.
--                d_delayed <= outputstreams;
    
--                for i in d_delayed'range loop
--                    test_tdata(i*32 + 31 downto i*32) <= d_delayed(i).data;
--                end loop;
--                test_tvalid <= d_delayed(0).valid;
--                test_tlast  <= d_delayed(0).valid and not outputstreams(0).valid;
--            end if;
--        end process;

--    q_buffer(0) <= q_int;

--    remove_crc_lid : process(clk)
--    begin
--        -- TODO: If reset set remove_next_two to 0.
--        if clk'event and clk = '1' then
--            q_buffer(q_buffer'high downto 1) <= q_buffer(q_buffer'high-1 downto 0);
--            for i in q_buffer(0)'range loop
--                if (q_buffer(q_buffer'high-1)(i).valid = '1' and q_buffer(q_buffer'high-2)(i).valid = '0' and q_buffer(q_buffer'high-5)(i).valid = '0') or    -- If we're at the end of a packet (last word was still valid) 
--                   (q_buffer(q_buffer'high)(i).valid = '1' and q_buffer(q_buffer'high-1)(i).valid = '0' and q_buffer(q_buffer'high-4)(i).valid = '0') then    -- remove the CRC and LID fields
--                    q(i).data   <= q_buffer(q_buffer'high-3)(i).data;
--                    q(i).strobe <= q_buffer(q_buffer'high-3)(i).strobe;
--                    q(i).done   <= q_buffer(q_buffer'high-3)(i).done;
--                    q(i).valid  <= '0';
----                elsif q_buffer(q_buffer'high-2)(i).valid = '0' and q_buffer(q_buffer'high-4)(i).valid = '0' then    -- If there's a stray valid in the invalid gap we remove it. 
----                    q(i).data   <= q_buffer(q_buffer'high-3)(i).data;
----                    q(i).strobe <= q_buffer(q_buffer'high-3)(i).strobe;
----                    q(i).done   <= q_buffer(q_buffer'high-3)(i).done;
----                    q(i).valid  <= '0';  -- Overriding the valid bit if a padding word was found in the comma gap
--                else
--                    q(i) <= q_buffer(q_buffer'high-3)(i);
--                end if;
--            end loop;
--        end if;
--    end process; 

    gap_cleaner : entity work.comma_gap_cleaner
        generic map (
            QUADS => NSTREAMS/4
        )
        port map (
            clk => clk,
            rst => rst_algo,
            d   => q_int,
            q   => q
        );

    inputs : entity work.inputs
        generic map (
            ORBIT => ORBIT,
            QUADS => NSTREAMS/4,
            BYPASS_LINKS => true
        )
        port map(
            axi_clk     => clk,
            mgtrefclk0  => '0',
--            mgtrefclk0_p  => mgtrefclk0_x0y3_p,
--            mgtrefclk0_n  => mgtrefclk0_x0y3_n,
                    -- Serial data ports for transceiver channel 0
            ch0_gthrxn_in  => '1',
            ch0_gthrxp_in  => '1',
            ch0_gthtxn_out => open,
            ch0_gthtxp_out => open,
          
            -- Serial data ports for transceiver channel 1
            ch1_gthrxn_in  => '1',
            ch1_gthrxp_in  => '1',
            ch1_gthtxn_out => open,
            ch1_gthtxp_out => open,
          
            -- Serial data ports for transceiver channel 2
            ch2_gthrxn_in  => '1',
            ch2_gthrxp_in  => '1',
            ch2_gthtxn_out => open,
            ch2_gthtxp_out => open,
          
            -- Serial data ports for transceiver channel 3
            ch3_gthrxn_in  => '1',
            ch3_gthrxp_in  => '1',
            ch3_gthtxn_out => open,
            ch3_gthtxp_out => open,

            clk_freerun    => clk,
            rst_freerun    => '1',
            clk_freerun_buf => open, -- Clock after being globally buffered
--            link_status_out     => link_status_out,
--            link_down_latched_out     => link_down_latched_out,
            init_done_int     => open,
            init_retry_ctr_int     => open,
            gtpowergood_vio_sync     => open,
            txpmaresetdone_vio_sync     => open,
            rxpmaresetdone_vio_sync     => open,
            gtwiz_reset_tx_done_vio_sync     => open,
            gtwiz_reset_rx_done_vio_sync     => open,
            hb_gtwiz_reset_all_vio_int    => "0",
            hb0_gtwiz_reset_tx_pll_and_datapath_int    => "0",
            hb0_gtwiz_reset_tx_datapath_int    => "0",
            hb_gtwiz_reset_rx_pll_and_datapath_vio_int    => "0",
            hb_gtwiz_reset_rx_datapath_vio_int    => "0",
            link_down_latched_reset_vio_int    => "0",
            loopback    => (others => '0'),
            
            oLinkData => open,

            oEvt_word => open,
            oRxbyteisaligned => open,
            cdr_stable => open,
            oBcounter => open,
            oOcounter => open,
            iReady => '0',

            clk            => open,
            q              => q_int,
            oCommaDet      => open,
            status_ok      => open,
            status_locked  => open
            );

--    create_testdata : process(clk)
--        variable send_padding : std_logic;
--    begin
--        if clk'event and clk = '1' then
--            if (bcounter mod 4 = 0) and (evt_word = 0) and (send_padding = '0') then
--                send_padding := '1';
--            elsif evt_word < 5 then
--                send_padding := '0';
--                evt_word     <= evt_word+1;
--            elsif evt_word = 5 then
--                send_padding := '0';
--                evt_word     <= 0;
--                if bcounter < ORBIT then
--                    bcounter <= bcounter+1;
--                elsif bcounter = ORBIT then
--                    bcounter <= 1;
--                    ocounter <= ocounter+1;
--                else
--                    bcounter <= 1;
--                end if;
--            else
--                send_padding := '0';
--                evt_word     <= 0;
--            end if;



--            -- Create test data now
--            for i in q_buffer(0)'range loop         -- Number of channels
--                if send_padding = '1' then
--                    q_int(i).data   <= x"F7F7F7F7"; -- Padding
--                    q_int(i).valid  <= '1';
--                    q_int(i).strobe <= '0';
--                    q_int(i).done   <= '0';
--                elsif bcounter > i and bcounter < ORBIT+i-10 then -- Arbitrary obit gap with desynced links
----                    if evt_word = 0 then
----                        inputstreams(i).data  <= std_logic_vector(to_unsigned(bcounter, 32));
----                    elsif evt_word = 1 then
----                        inputstreams(i).data  <= std_logic_vector(to_unsigned(ocounter, 32));
----                    elsif (evt_word = 2) or (evt_word = 4) then
----                        inputstreams(i).data  <= "000000001" & "1100" & "100000000" & "0000000000";
----                    elsif (evt_word = 3) or (evt_word = 5) then
------                        tx_data(i*32 + 31 downto i*32)  <= "00000000000" & "0000000001" & "0111111" & "10" & "00";
----                        inputstreams(i).data  <= (others => '1');
------                        inputstreams(i).data  <= "00000000000" & "0000000001" & "0111111" & "10" & "00";
----                    else
----                        inputstreams(i).data  <= (others => '1');
----                    end if;
--                    q_int(i).data   <= std_logic_vector(to_unsigned(evt_word, 16)) & std_logic_vector(to_unsigned(bcounter-i, 16));
--                    q_int(i).valid  <= '1';
--                    q_int(i).strobe <= '1';
--                    if bcounter = ORBIT-4 and evt_word = 5 then
--                        q_int(i).done  <= '1';
--                    else
--                        q_int(i).done  <= '0';
--                    end if;
--                elsif bcounter = ORBIT+i-10 and (evt_word = 1 or evt_word = 2) then
--                    q_int(i).done   <= '0';
--                    q_int(i).strobe <= '1';
--                    q_int(i).valid  <= '1';
--                    q_int(i).data   <= "10101010101010101010101010101010"; -- If I see this pattern I'm transmitting "CRCs".
--                else
--                    q_int(i).done   <= '0';
--                    q_int(i).strobe <= '1';
--                    q_int(i).valid  <= '0';
--                    q_int(i).data   <= x"505050BC"; -- COMMA
--                end if;
--            end loop;

--        end if;
--    end process;


  runtb : process
    variable i : integer := 0;
    variable j : integer := 0;

  begin
    rst_global <= '1';
    start_align <= '0';
    test_tready <= '1';

--    for j in 0 to NSTREAMS-1 loop
--      inputstreams(j).data <= (others => '0');
--      inputstreams(j).valid <= '0';
--    end loop;

    for i in 0 to 2 loop
      wait for 2*t_halfperiod;
    end loop;

    rst_global <= '0';

    for i in 0 to 6*3*ORBIT loop
      wait for 2*t_halfperiod;
    end loop;
    
    for i in 1 to 6*ORBIT loop

      -- change signals
      if i=5 then
--        reset <= '1';
        bc0 <= '1';
      else
--        reset <= '0';
        bc0 <= '0';
      end if;

      if i > 39 and i < 59 then
          test_tready <= '0';
      elsif i > 130 and i < 131 then
          test_tready <= '0';
      elsif i > 145 and i < 147 then
          test_tready <= '0';
      elsif i > 160 and i < 163 then
          test_tready <= '0';
      elsif i > 175 and i < 179 then
          test_tready <= '0';
      elsif i > 195 and i < 200 then
          test_tready <= '0';
      elsif i > 220 and i < 226 then
          test_tready <= '0';      
      elsif i > 250 and i < 257 then
          test_tready <= '0';
      elsif i > 271 and i < 272 then
          test_tready <= '0';
      elsif i > 273 and i < 274 then
          test_tready <= '0';
      else
          test_tready <= '1';
      end if;

--      if i=10 then
--        start_align <= '1';
--      else
--        start_align <= '0';
--      end if;


--     for j in 0 to NSTREAMS -1 loop
--        if i > 20+7*j and i < 150+7*j then
--          inputstreams(j).valid <= '1';
--          inputstreams(j).data <= STD_LOGIC_VECTOR(TO_UNSIGNED(i-(20+7*j),inputstreams(j).data'length)); 
--        elsif i > 149+7*j and i < 270+7*j then
--          inputstreams(j).valid <= '0';
--          inputstreams(j).data <= STD_LOGIC_VECTOR(TO_UNSIGNED(i-(20+7*j),inputstreams(j).data'length)); 
--        elsif i > 269+7*j and i < 390+7*j then
--          inputstreams(j).valid <= '1';
--          inputstreams(j).data <= STD_LOGIC_VECTOR(TO_UNSIGNED(i-(20+7*j),inputstreams(j).data'length)); 
--        end if;
--      end loop;  
      

      
      wait for 2*t_halfperiod;
    end loop;

  end process runtb;  
  

end Behavioral;